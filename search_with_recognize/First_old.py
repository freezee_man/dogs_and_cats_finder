from imageai.Detection import ObjectDetection
import os


def main():
    execution_path = os.getcwd()
    print(execution_path)
    detector = ObjectDetection()
    detector.setModelTypeAsYOLOv3()
    detector.setModelPath( os.path.join(execution_path , "yolo.h5"))
    detector.loadModel(detection_speed="fast")

    custom_objects = detector.CustomObjects(dog=True, cat=True)
    files_list  = os.listdir(execution_path + "/photos")
    print(files_list)
    # def find_in_image(detector):

    detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "image.jpg"), output_image_path=os.path.join(execution_path , "imagenew.jpg"), minimum_percentage_probability=30)
    for eachObject in detections:
        print(eachObject["name"] , " : " , eachObject["percentage_probability"] )


if __name__ == '__main__':
    main()
