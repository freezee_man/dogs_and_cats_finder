# -*- coding: utf-8 -*-
import vk_api
import requests
import threading
import os
import progressbar


totalinc = 0
novalid = 0


def all_do(vk_session, file):
    global totalinc, novalid
    bar = progressbar.ProgressBar(max_value=9899809)
    f = open('base/' + file, 'r')
    for i in f:
        u_id = i[0:-1]
        back_info, back_url = photo_url(vk_session, u_id)
        if back_info:
            photo_save(back_url, u_id)
        else:
            novalid = novalid + 1
        totalinc = totalinc + 1
        bar.update(totalinc)


    f.close()


def photo_save(url, name):
    p = requests.get(url)
    out = open('photos/' + name + '.jpg', 'wb')
    out.write(p.content)
    out.close()


def photo_url(vk_session, u_id):
    vk = vk_session.get_api()

    response = vk.users.get(user_id=u_id, fields='has_photo, photo_max_orig')
    # print(response[0]['photo_max_orig'])
    return response[0]['has_photo'], response[0]['photo_max_orig']


def auth_handler():
    """ При двухфакторной аутентификации вызывается эта функция.
    """

    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")
    # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device


def main():
    global novalid
    login, password = 'freezee_man@21region.org', 'dima2016'
    vk_session = vk_api.VkApi(
        login, password,
        # функция для обработки двухфакторной аутентификации
        auth_handler=auth_handler,
        scope='wall'
    )

    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    threads = []

    files = os.listdir('base/')
    files = files[1:]

    for ll in files:
        threads.append(threading.Thread(target=all_do, args=(vk_session, ll, )))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print('Total no Valid = ' + novalid)


if __name__ == '__main__':
    main()
