# -*- coding: utf-8 -*-
from imageai.Detection import ObjectDetection # Библиотека распознавания объектов
import os  # Иморт библиотеки ос питон
import vk_api #подкл к апи вк
import requests # библ обр запросов к сайтам
# import threading
import progressbar # библ прогресс бар

totalinc = 0 # сколько пройдено id
novalid = 0 #сколько не валидных профилей без фото


execution_path = os.getcwd() #подкл к текущей директории где все файлы
print('Load current path: ' + execution_path)
detector = ObjectDetection() #иницилизация модели распознавания
detector.setModelTypeAsYOLOv3() #выбор модели распозн
detector.setModelPath(os.path.join(execution_path, "models/yolo.h5"))# подключение к самой модели
detector.loadModel(detection_speed="fast") #скорость раб модели
custom = detector.CustomObjects(dog=True, cat=True) #выборка необходимых объектов на фото


def move_photo(u_id):
    if not os.path.exists(execution_path + '/photos'):
        os.makedirs(execution_path + '/photos')
    file1 = '.temp/' + u_id + '.jpg'
    file2 = '.temp/' + u_id + '_recognized.jpg'

    os.rename(file1, execution_path + '/photos/' + u_id + '.jpg')
    os.rename(file2, execution_path + '/photos/' + u_id + '_recognized.jpg')


def all_do(vk_session, file): #функция обработки профилей
    global totalinc, novalid
    # detector, custom = open_model_detection()
    bar = progressbar.ProgressBar(max_value=999924)
    #f = open('base/' + file, 'r')
    f = open(file)
    for i in f:
        totalinc = totalinc + 1
        bar.update(totalinc)
        u_id = i[0:-1]
        back_info, back_url = photo_url(vk_session, u_id)
        if back_info:
            photo_save(back_url, u_id)
            image_recognize_and_find(u_id, detector)
        else:
            novalid = novalid + 1
    f.close()


def photo_save(url, name): #функция сохраниения фото
    p = requests.get(url)
    if not os.path.exists(execution_path + '/.temp'):
        os.makedirs(execution_path + '/.temp')
    out = open('.temp/' + name + '.jpg', 'wb')
    out.write(p.content)
    out.close()


def photo_url(vk_session, u_id): #получение id фото
    vk = vk_session.get_api()

    response = vk.users.get(user_id=int(u_id), fields='has_photo, photo_max_orig')
    # print(response[0]['photo_max_orig'])
    return response[0]['has_photo'], response[0]['photo_max_orig']


def auth_handler(): #обработка 2 факторной аудентификации
    """ При двухфакторной аутентификации вызывается эта функция.
    """

    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")
    # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device


def main(): #главная функция // инициализация входа вк и отдача всех данных на верхнюю функцию
    global novalid
    login, password = 'freezee_man@21region.org', 'dima2016'
    vk_session = vk_api.VkApi(
        login, password,
        # функция для обработки двухфакторной аутентификации
        auth_handler=auth_handler,
        scope='wall'
    )

    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    threads = []

    files = os.listdir('base/')
    # files = files[1:]
    print(files)


    # for ll in files:
    #     threads.append(threading.Thread(target=all_do, args=(vk_session, "base/" + ll, )))
    #
    # for thread in threads:
    #     thread.start()
    #
    # for thread in threads:
    #     thread.join()
    for ll in files:
        all_do(vk_session, 'base/' + ll)
    print('Total no Valid = ' + str(novalid))


def image_recognize_and_find(u_id, detector): #поиск объектов на изображении
    count = 0
    file1 = '.temp/' + u_id + '.jpg'
    file2 = '.temp/' + u_id + '_recognized.jpg'
    detections = detector.detectCustomObjectsFromImage(custom_objects=custom, input_image=file1, output_image_path=file2, minimum_percentage_probability=30)

    for eachObject in detections:
        if eachObject["name"] == "dog" or eachObject["name"] == "cat":
            count = count + 1

    if count == 0:
        os.remove(file1)
        os.remove(file2)
    else:
        move_photo(u_id)


if __name__ == '__main__':
    main()
